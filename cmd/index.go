/*
Copyright © 2022
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/wirawirw/aksara-cli/cmd/builder"
	reader2 "gitlab.com/wirawirw/aksara-cli/cmd/reader"
)

var generatorCmd = &cobra.Command{
	Use:   "generate",
	Short: "Auto generate Design Pattern",
	Long:  `Some folks say that Design Patterns are dead. How foolish. The Design Patterns book is one of the most important books published in our industry.  The concepts within should be common rudimentary knowledge for all professional programmers.`,
	Run: func(cmd *cobra.Command, args []string) {
		builder := builder.Setup(args[0])
		reader := reader2.NewReader()
		reader.ReadModel(builder)
	},
}

func init() {
	rootCmd.AddCommand(generatorCmd)
}
