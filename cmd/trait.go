package reader

import (
	"bufio"
	"gitlab.com/wirawirw/aksara-cli/backup/tool"
	"gitlab.com/wirawirw/aksara-cli/cmd"
	"io"
	"strings"
)

type Blueprint struct {
	ModuleName string  `json:"module_name"`
	ResultPath string  `json:"result_path"`
	Jobs       []Trait `json:"jobs"`
}

type Trait struct {
	Meta map[string]interface{} `json:"meta"`
	ModelTrait
	cmd.ModuleTrait
}

type ModelTrait struct {
	ModelFields    []ModelField `json:"model_fields"`
	ModelName      string       `json:"model_name"`
	ModelNameSnake string       `json:"model_name_snake"`
}

type ModelField struct {
	Json  string `json:"json"`
	Name  string `json:"name"`
	Type  string `json:"type"`
	IsPtr bool   `json:"is_ptr"`
}

func (r *ModelTrait) extract(fl io.Reader) {
	scanner := bufio.NewScanner(fl)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "~ignore") {
			continue
		}

		if strings.Contains(scanner.Text(), "json") {
			line := strings.Fields(scanner.Text())
			isPtr := strings.Contains(line[1], "*")
			r.ModelFields = append(r.ModelFields, ModelField{
				Json:  tool.ToSnakeCase(line[0]),
				Name:  line[0],
				Type:  strings.Replace(line[1], "*", "", 1),
				IsPtr: isPtr,
			})
		}

	}
	return
}

func NewModelTraitFromFile(fl io.Reader, modelName string) *ModelTrait {
	m := &ModelTrait{
		ModelName:      modelName,
		ModelNameSnake: tool.ToSnakeCase(modelName),
	}

	m.extract(fl)
	return m
}
