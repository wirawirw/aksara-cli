package model

type Company struct {
	Name        string  `json:"name" adb:"Name" validate:"required"`
	Description *string `json:"description" adb:"Description" validate:""`
}
